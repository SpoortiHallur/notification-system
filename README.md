# Notification System - centralized generic service for notification


### Supported channels

These channels are mocked for test purpose

* Email
* SMS

## Why you may want to use this notification system

Most applications have the need to implement notifications for a variety of use cases and scenarios. Notification system is a centralized generic service for notification that can be used by a variety consuming application for their notification needs e.g. an incident workflow system may use this system when each incident ticket moves from one state to another, similarly a order management system may use this service to notify the customer of the status of the order whenever it changes


## Supported methods


### `POST`

Post Api is exposed to the clients for sending the messages to the notification service. A well defined json format is defined as an interface between client and service. Plese refer the example request in "Testing in Postman" section.

## Getting started

The code is created as gradle springboot project

```sh
gradlew clean build
java -jar /build/libs/demo-0.0.1-SNAPSHOT.jar
```

The application gets deployed.


## Setup

### Testing in Postman

To setup the postman, you need to perform three steps:

1. Install Postman plugin
2. Endpoint - http://localhost:8080/notification
2. Body - 
	{
	"from":"Spoorti",
 	"to":"9844842417",
 	"payload":"Welcome to Target",
 	"channel":"sms"
}
3. text type - applicatio/json

Based on the request - The parameters are validated by the notification service and appropriate response is sent back to the client.

## Architecture

![screenshot](demo/src/main/img/notification_architecture.jpeg)

### REST endpoints

Currently only the POST API is supported. Details of the APIs is available in "Testing in Postman" section. The API acts as the service which accepts messages from various clients and dispatches the message to the message broker for transmitting the message to the suppied channel.Please check NotificationController.java for more details.

### Channel Service

The channel service acts as an interface between the notification service and the message broker. The notification service validates the incomming message and supplies to the channel service which in turn pushes the message down to message broker. In the code - the SMSChannelService extends IChannelService and implements the interface.

### Notification Service

Validates the incomming message and transmits the message to the channel service for processing

### Channel Factory

Creates Channel Service based on the channel interface provided in the request by the clients.

### Exception handling

NotificationService validates the incomming message and supplies the error cases to ExcepionHandler for appropriate error handling. Currently the invalid channel is handled as part of error handling. A 400 bad request is sent back from NotificationService in the following scenario. Please refer NotificationControllerAdvice and UnsupportedException for details.

#### Sample error response
{"errorCode":"SRV_UNSUPPORTED","errorMessage":"Unsupported request"}

### No Sql DB

Currently there is no DB. DB can be planned as part of next MVP for handling error scenarios.

### Message Broker Integration

Currently there is no mesage broker, instead the message sent from the client is submitted to mock message broker. Assumption is that, the message broker will take care of further sending notifications to the supplied channel in the order of message recieved.

### Text Processing Engine

This is again not part of this deliverable. This is intended and kept for validating the messages from client in accordance with the channel supplied.

### Outbound Consumer

This module is intended for recieving success/failure responses from message broker.




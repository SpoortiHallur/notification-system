package com.example.advice;

import com.example.dto.ErrorDto;
import com.example.exception.UnSupportedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This class catches the exceptions thrown by {@link com.example.rest.resources.NotificationController} class
 */
@ControllerAdvice
public class NotificationControllerAdvice {

    /**
     * @return returns response entity with appropriate error
     */
    @ExceptionHandler(value = UnSupportedException.class)
    public ResponseEntity<ErrorDto> handleUnsupportedException(){

        ErrorDto error =  new ErrorDto("SRV_UNSUPPORTED","Unsupported request");

        return  new ResponseEntity(error,null,HttpStatus.BAD_REQUEST);

    }
}

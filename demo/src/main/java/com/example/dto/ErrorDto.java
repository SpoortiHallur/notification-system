package com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Error Contract between client and Notification System
 */
@Data
@AllArgsConstructor
public class ErrorDto {

    private String errorCode;
    private String errorMessage;

}

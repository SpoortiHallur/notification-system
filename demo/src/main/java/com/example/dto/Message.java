package com.example.dto;

import lombok.Data;

/**
 * Message contract between client and Notification System
 */
@Data
public class Message {

    String from;
    String to;
    String payload;

    String channel;


}

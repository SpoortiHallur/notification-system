package com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Notification Result contract sent to the client
 */
@Data
@AllArgsConstructor
public class NotificationDto
{
    int notoficiationId;
    String status;
}

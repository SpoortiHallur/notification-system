package com.example.exception;

/**
 * Custom Exception for handling the message contract error between client and the notification system
 */
public class UnSupportedException extends  Exception{

    public UnSupportedException(String message){
        super(message);
    }
}

package com.example.rest.resources;

import com.example.dto.Message;
import com.example.dto.NotificationDto;
import com.example.service.INotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Exposes a POST API for the clients to use notification service.
 */
@Controller(value="/notification")
public class NotificationController {

    @Autowired
    INotificationService service;

    /**
     * @param message The message sent by the client
     * @return Response entity of the type {@link NotificationDto}
     * @throws Exception Response entity of the type {@link com.example.dto.ErrorDto}
     */
    @PostMapping
    public ResponseEntity<NotificationDto> create(@RequestBody Message message) throws Exception {
        NotificationDto notificationDto= service.process(message);
        return ResponseEntity.ok(notificationDto);
    }
}

package com.example.service;

import com.example.exception.UnSupportedException;
import com.example.service.impl.EmailChannelService;
import com.example.service.impl.SMSChannelService;
import org.springframework.stereotype.Component;

/**
 * Factory for returning the desired Channel Service based on the client request
 */
@Component
public class ChannelFactory {
    private static final String SMS = "SMS";
    private static final String EMAIL = "EMAIL";

    /**
     * @param channel The channel supplied by client
     * @return creates and returned the channel requested
     * @throws Exception returns error in case of unsupported channel in the format of {@link com.example.dto.ErrorDto}
     */
    public IChannelService getChannelService(String channel) throws Exception {
        switch (channel.toUpperCase()) {

            case SMS:
                return new SMSChannelService();
            case EMAIL:
                return new EmailChannelService();
            default:

                throw new UnSupportedException("Unsupported Channel type");

        }

    }
}

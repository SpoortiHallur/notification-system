package com.example.service;

import com.example.dto.Message;
import com.example.dto.NotificationDto;

public interface IChannelService {

    public NotificationDto submit(Message message);
}

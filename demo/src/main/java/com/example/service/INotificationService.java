package com.example.service;

import com.example.dto.Message;
import com.example.dto.NotificationDto;

public interface INotificationService {

    public NotificationDto process(Message message) throws Exception;

}

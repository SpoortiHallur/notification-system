package com.example.service.impl;

import com.example.dto.Message;
import com.example.dto.NotificationDto;
import com.example.service.IChannelService;

/**
 * Used for submitting the email job to message broker for sending it to Email Client
 * Please note that the message broker is a virtual or mocked entity
 * The message object validations required for email client are out of scope for this MVP.
 */
public class EmailChannelService implements IChannelService {
    /**
     * @param message The message received from the client (In our test case - its Postman)
     * @return Sends the result to notification system about the success/failure case from message broker
     * Please note: As of now considering mocked message broker - always a success is returned
     */
    @Override
    public NotificationDto submit(Message message) {
        return new NotificationDto(message.getPayload().length(), "Success");
    }
}

package com.example.service.impl;

import com.example.dto.Message;
import com.example.dto.NotificationDto;
import com.example.exception.UnSupportedException;
import com.example.service.ChannelFactory;
import com.example.service.IChannelService;
import com.example.service.INotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * A Service that validates the incoming message
 */
@Service
public class NotificationService implements INotificationService {

    /**
     * List of supported channels. This can be made as part of the configuration instead of hardcoding here.
     */
    @Value("${notifciation.channels:SMS,EMAIL,SLACK}")
    List<String> supportedChannels;

    /**
     * A Factory that returns the desired channelService based on the message sent by clients
     */
    @Autowired
    ChannelFactory factory;

    /**
     * @param message The message recieved by clients
     * @return returns the result to the client in the format of {@link NotificationDto}
     * @throws Exception returns the error to the client in the format of {@link com.example.dto.ErrorDto}
     */
    public NotificationDto process(Message message) throws Exception {
        if (!preCondition(message)) {
            throw new UnSupportedException("Unsupported channel");
        }
        IChannelService service = factory.getChannelService(message.getChannel());
        return service.submit(message);


    }

    /**
     * @param message The message sent by the clients
     * @return returns true if the condition is met. As of now only 2 pre-conditions are being validated.
     * First - Channel Empty
     * Second - Channel not supported
     * @throws UnSupportedException returns an error object in the format of {@link com.example.dto.ErrorDto}
     */
    private boolean preCondition(Message message) throws UnSupportedException {
        if (Objects.isNull(message.getChannel()))
            throw new UnSupportedException("Channel Cannot be empty");
        return supportedChannels.contains(message.getChannel().toUpperCase());
    }

}

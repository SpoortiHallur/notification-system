package com.example.service.impl;

import com.example.dto.Message;
import com.example.dto.NotificationDto;
import com.example.service.IChannelService;

/**
 * Used for submitting the SMS job to message broker for sending it to SMS Client
 * Please note that the message broker is a virtual or mocked entity
 * The message object validations required for SMS client are out of scope for this MVP.
 */
public class SMSChannelService implements IChannelService {

    /**
     * @param message The message received from the client (In our test case - its Postman)
     * @return Sends the result to notification system about the success/failure case from message broker
     * Please note: As of now considering mocked message broker - always a success is returned
     */
    @Override
    public NotificationDto submit(Message message) {
        NotificationDto notificationDto = new NotificationDto(message.getPayload().length(), "Success");
        return notificationDto;
    }
}

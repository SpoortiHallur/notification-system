package com.example.advice;

import com.example.dto.ErrorDto;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

public class NotificationControllerAdviceTest {

    NotificationControllerAdvice notificationControllerAdvice;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        notificationControllerAdvice = new NotificationControllerAdvice();
    }

    @After
    public void tearDown() throws Exception {
        notificationControllerAdvice = null;
    }

    @Test
    public void handleUnsupportedException() {
        ResponseEntity<ErrorDto> errorDtoResponseEntity = notificationControllerAdvice.handleUnsupportedException();
        assert errorDtoResponseEntity.getBody().getErrorCode().equalsIgnoreCase("SRV_UNSUPPORTED");
    }
}
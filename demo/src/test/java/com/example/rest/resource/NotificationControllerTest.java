package com.example.rest.resource;

import com.example.demo.DemoApplication;
import com.example.dto.NotificationDto;
import com.example.exception.UnSupportedException;
import com.example.service.INotificationService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest(classes= DemoApplication.class)
public class NotificationControllerTest
{
    @Mock
    INotificationService service;

    @Autowired
    private WebApplicationContext webApplicationContext;


    private MockMvc mockMvc;

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void createTestWithEmptyBody() throws Exception {
        try {
            Mockito.when(service.process(Mockito.any())).thenReturn(new NotificationDto(1, "SUCCESS"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBuilder builder = MockMvcRequestBuilders.post("/notification").accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(builder).andReturn();

        Assert.assertEquals(400, result.getResponse().getStatus());

    }

    @Test
    public void createTestWithBody() throws Exception {
        try {
            Mockito.when(service.process(Mockito.any())).thenReturn(new NotificationDto(1, "SUCCESS"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBuilder builder = MockMvcRequestBuilders.post("/notification").contentType(MediaType.APPLICATION_JSON).
                content("{\n" +
                        "\t\"from\":\"Veeresh\",\n" +
                        "  \"to\":\"Test\",\n" +
                        "  \"payload\":\"Vardhan\",\n" +
                        "  \"channel\":\"sms\"\n" +
                        "}")
                ;
        MvcResult result = mockMvc.perform(builder).andReturn();

        Assert.assertEquals(200, result.getResponse().getStatus());

    }

    @Test
    public void creatWithInvalidChannel() throws Exception{
        try {
            Mockito.when(service.process(Mockito.any())).thenThrow(new UnSupportedException("Unsupported channel"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBuilder builder = MockMvcRequestBuilders.post("/notification").contentType(MediaType.APPLICATION_JSON).
                content("{\n" +
                        "\t\"from\":\"Veeresh\",\n" +
                        "  \"to\":\"Test\",\n" +
                        "  \"payload\":\"Vardhan\",\n" +
                        "  \"channel\":\"adsfas\"\n" +
                        "}")
                ;
        MvcResult result = mockMvc.perform(builder).andReturn();

        Assert.assertEquals(400, result.getResponse().getStatus());

    }

    @Test
    public void createWithUnsupportedChannel() throws Exception{
        try {
            Mockito.when(service.process(Mockito.any())).thenThrow(new UnSupportedException("Unsupported channel"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBuilder builder = MockMvcRequestBuilders.post("/notification").contentType(MediaType.APPLICATION_JSON).
                content("{\n" +
                        "\t\"from\":\"Veeresh\",\n" +
                        "  \"to\":\"Test\",\n" +
                        "  \"payload\":\"Vardhan\",\n" +
                        "  \"channel\":\"slack\"\n" +
                        "}")
                ;
        MvcResult result = mockMvc.perform(builder).andReturn();

        Assert.assertEquals(400, result.getResponse().getStatus());

    }
}

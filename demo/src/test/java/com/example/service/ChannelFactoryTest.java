package com.example.service;

import com.example.exception.UnSupportedException;
import com.example.service.impl.EmailChannelService;
import com.example.service.impl.SMSChannelService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@WebAppConfiguration
@SpringBootTest
public class ChannelFactoryTest {


    @InjectMocks
    private ChannelFactory factory;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void getValidSMSChannelService() throws Exception {
        IChannelService service =  factory.getChannelService("sms");
        Assert.assertTrue(service instanceof SMSChannelService);
    }

    @Test(expected= UnSupportedException.class)
    public void getInvalidChannelService() throws Exception
    {
        IChannelService service = factory.getChannelService("aaaa");
    }

    @Test
    public void getValidEmailChannelService() throws Exception
    {
        IChannelService service = factory.getChannelService("Email");
        Assert.assertTrue(service instanceof EmailChannelService);
    }

    @Test(expected= UnSupportedException.class)
    public void getUnsupportedChannelService() throws Exception
    {
        IChannelService service = factory.getChannelService("slack");
    }
}

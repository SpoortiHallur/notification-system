package com.example.service.impl;

import com.example.dto.Message;
import com.example.dto.NotificationDto;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

public class EmailChannelServiceTest {
    EmailChannelService emailChannelService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        emailChannelService = new EmailChannelService();
    }

    @After
    public void tearDown() throws Exception {
        emailChannelService = null;
    }

    @Test
    public void submit() {
        Message message = new Message();
        message.setChannel("email");
        message.setTo("Spoorti");
        message.setFrom("target");
        message.setPayload("Welcome to target0");
        NotificationDto notificationDto = emailChannelService.submit(message);
        assertEquals(message.getPayload().length(),notificationDto.getNotoficiationId());
    }
}
package com.example.service.impl;

import com.example.dto.Message;
import com.example.exception.UnSupportedException;
import com.example.service.ChannelFactory;
import com.example.service.IChannelService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NotificationServiceTest {
    NotificationService notificationService;

    @Mock
    ChannelFactory channelFactoryMock;

    @Mock
    IChannelService channelServiceMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        notificationService = new NotificationService();
        List channels = new ArrayList();
        channels.add("EMAIL");
        channels.add("SMS");
        notificationService.supportedChannels = channels;
        notificationService.factory = channelFactoryMock;
    }

    @After
    public void tearDown() throws Exception {
        notificationService = null;
        channelFactoryMock = null;
        channelServiceMock = null;
    }

    @Test
    public void processValidMessage() {
        Message message = new Message();
        message.setChannel("sms");
        message.setTo("9844842427");
        message.setFrom("9844796263");
        message.setPayload("Welcome to target");

        try {
            when(channelFactoryMock.getChannelService(message.getChannel())).thenReturn(channelServiceMock);
            notificationService.process(message);
            verify(channelServiceMock).submit(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(expected = UnSupportedException.class)
    public void processEmptyMessageChannel() throws Exception {
        Message message = new Message();
        message.setTo("9844842427");
        message.setFrom("9844796263");
        message.setPayload("Welcome to target");
        notificationService.process(message);
    }

    @Test(expected = UnSupportedException.class)
    public void processInvalidMessageChannel() throws Exception {
        Message message = new Message();
        message.setTo("9844842427");
        message.setFrom("9844796263");
        message.setChannel("slack");
        message.setPayload("Welcome to target");
        notificationService.process(message);
    }

}
package com.example.service.impl;

import com.example.dto.Message;
import com.example.dto.NotificationDto;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

public class SMSChannelServiceTest {
    SMSChannelService smsChannelService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        smsChannelService = new SMSChannelService();
    }

    @After
    public void tearDown() throws Exception {
        smsChannelService = null;
    }

    @Test
    public void submit() {
        Message message = new Message();
        message.setChannel("sms");
        message.setTo("9844842427");
        message.setFrom("9844796263");
        message.setPayload("Welcome to target");
        NotificationDto notificationDto = smsChannelService.submit(message);
        assertEquals(message.getPayload().length(),notificationDto.getNotoficiationId());
    }
}